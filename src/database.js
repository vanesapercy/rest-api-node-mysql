//Conectarnos a la B.D
const mysql = require('mysql');

const mysqlConnection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'MySecret12*',
    database: 'company',
    multipleStatements: true
    
});

mysqlConnection.connect(function (err){
    if(err){
        console.log(err);
        return;

    }else{
        console.log('Db is connected');
    }

});

//Exportar el módulo para usarlo en otras partes de la App
module.exports = mysqlConnection;