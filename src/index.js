//Requerir el módulo express y guardarlo en una constante
const express = require('express');
const app = express();

//Settings
app.set('port', process.env.PORT || 3000);

//Middlewares
app.use(express.json());

//Routes - Para comunicar el servidor con el navegador
app.use(require('./routes/employees'));

//Iniciando el servidor
app.listen(app.get('port'),  () => {
    console.log('Server on port', app.get('port' ));
});


